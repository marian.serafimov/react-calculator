import React, { useState } from 'react';

function History(props) {
    const[isHistoryOpen, setIsHistoryOpen] = useState(false);
    const[historyHeight, setHistoryHeight] = useState(0);


    function togglBtn() {
      if(isHistoryOpen){
        setIsHistoryOpen(false);
        setHistoryHeight(0);
      }else{
        setIsHistoryOpen(true);
        setHistoryHeight(100);
      }
    }
  return (
    <div className="history-section">
        <div className="dropdown"  onClick={e => togglBtn()}>History ></div>
        <div className="history" style={{height: historyHeight + "px"}}>histories</div>
    </div>
  );
}

export default History;
