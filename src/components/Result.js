import React from 'react';

function Result(props) {
  return (
    <div className="result" style={{height: props.resHeight + 'px'}} >
        <div className="result-text">
          <span>{props.result}</span><span className="blink">_</span>
        </div>
    </div>
  );
}

export default Result;
