import React from 'react';

function KeyPad(props) {
  function doClick(target) {
    if(target === 'CE'){
        props.setResult(props.result.substring(0, props.result.length - 1))
    }else if(target === 'C'){
        props.setResHeight(55);
        props.setResult(' ');
    }else if(target === "="){
        try {
            if(eval(props.result) == undefined){
                props.setResult("error")
            }else{
                props.setResult(eval(props.result).toString())
            }
        } catch (e) {
            props.setResult("error")
        }
    }else if(target == 'undefined'){
        props.setResult(' ');
    }else{
        props.setResHeight(100);

        if(props.result == 'error' || props.result == '0'){
            props.setResult(target);
        }else{
            props.setResult(props.result + target);
        }
    }
  }

  return (
    <div className="key-pad">
        <button className="grey" name="(" onClick={e => doClick(e.target.name)}>(</button>
        <button className="grey" name="CE" onClick={e => doClick(e.target.name)}>CE</button>
        <button className="grey" name=")" onClick={e => doClick(e.target.name)}>)</button>
        <button className="orange" name="C" onClick={e => doClick(e.target.name)}>C</button><br/>

        <button name="1" onClick={e => doClick(e.target.name)}>1</button>
        <button name="2" onClick={e => doClick(e.target.name)}>2</button>
        <button name="3" onClick={e => doClick(e.target.name)}>3</button>
        <button className="orange" name="+" onClick={e => doClick(e.target.name)}>+</button><br/>

        <button name="4" onClick={e => doClick(e.target.name)}>4</button>
        <button name="5" onClick={e => doClick(e.target.name)}>5</button>
        <button name="6" onClick={e => doClick(e.target.name)}>6</button>
        <button className="orange" name="-" onClick={e => doClick(e.target.name)}>-</button><br/>

        <button name="7" onClick={e => doClick(e.target.name)}>7</button>
        <button name="8" onClick={e => doClick(e.target.name)}>8</button>
        <button name="9" onClick={e => doClick(e.target.name)}>9</button>
        <button className="orange" name="*" onClick={e => doClick(e.target.name)}>x</button><br/>

        <button className="grey" name="." onClick={e => doClick(e.target.name)}>.</button>
        <button name="0" onClick={e => doClick(e.target.name)}>0</button>
        <button className="grey" name="=" onClick={e => doClick(e.target.name)}>=</button>
        <button className="orange" name="/" onClick={e => doClick(e.target.name)}>÷</button><br/>
    </div>
  );
}

export default KeyPad;
