import React, { useState } from 'react';

import Result from './components/Result';
import KeyPad from './components/KeyPad';
import History from './components/History';

function App() {
  const [result, setResult] = useState('');
  const [resHeight, setResHeight] = useState(55);
  const [histories, setHistories] = useState();

  return (
    <div className="App">
      <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet"></link>
      <History /> 
      <div className></div>
      <Result resHeight={resHeight} result={result}/>
      <KeyPad setResHeight={setResHeight} result={result} setResult={setResult}/>
    </div>
  );
}

export default App;